<?php

// ==============================
// Composer autoloader if present
// ==============================
if (file_exists(__DIR__ . '/wp-content/vendor/autoload.php')) {
    define('USE_COMPOSER_AUTOLOADER', true);
    require_once __DIR__ . '/wp-content/vendor/autoload.php';
}

// ===================================================
// Load database info and local development parameters
// ===================================================
if (file_exists(__DIR__ . '/local-config.php')) {
    include(__DIR__ . '/local-config.php');
}

// ===================================================
// Initialize Situation defaults
// ===================================================
if (class_exists('\Situation\WPConfig')) {
    new \Situation\WPConfig(__DIR__);
}

// ================================================
// You almost certainly do not want to change these
// ================================================
define('DB_CHARSET', 'utf8mb4');
define('DB_COLLATE', '');

// ==============================================================
// Salts, for security
// Grab these from: https://api.wordpress.org/secret-key/1.1/salt
// ==============================================================
define('AUTH_KEY',         '5>~;uhG8pJfM>q-oU=y.^,OeC7lEa])]}Y^J3RKT/}mA?hZ+w7soo-s(O-DsB~.B');
define('SECURE_AUTH_KEY',  'ACahVDm gY|9pLH&Lb~k]GZL+-Rwo$Hw`HNKO<GrS/[MajU4R?.iK>kfX;)$k8cT');
define('LOGGED_IN_KEY',    'U608Hy L].p4CEGRf~B,,zvw`j`m]@e2BWyFASZZwvhEGi5+Oh{tMk8=]aQ-w{}{');
define('NONCE_KEY',        'bjx!Z!E>q%-)NP%-B5yE|%8KE!I2a|D<8WBR|]2h`q]qt3[cT^2=/c|lmq!{i+y_');
define('AUTH_SALT',        'LjEUX2eL%t9}4[+X)+Sw ?{WWG2-iPE_+Sy#}P!9L|Li98]#+FH&ys&L5{,:Oe-e');
define('SECURE_AUTH_SALT', 'lq4ffi<@NdR9`v2E1zapgV[u}Qs@Y^{z|BlJR+89xA>,U=I:5>b5<|Iqwi_Jhc,?');
define('LOGGED_IN_SALT',   'n.D9R]8]?2{`{E-FyP!p+g}s|g,aWiDY!rzWZ-H-(4|3-a2&NzF`pHA/?]so]JI~');
define('NONCE_SALT',       '-3!X08PhBz/*BYaMHg|j*>g2-YEWyb|wIe+D^[B;.Daz5mN<b z&P5LVou8idgU:');


// ==============================================================
// Table prefix
// Change this if you have multiple installs in the same database
// Changing default to 'sit_' to enhance security
// ==============================================================
$table_prefix = 'sit_';

// ================================
// Language
// Leave blank for American English
// ================================
define('WPLANG', '');

// ===================
// Bootstrap WordPress
// ===================
if (! defined('ABSPATH')) {
    define('ABSPATH', dirname(__FILE__) . '/wp/');
}
require_once(ABSPATH . 'wp-settings.php');
