<?php

namespace ChildTheme\Service;

use Backstage\Models\PostBase;

/**
 * Class Service
 * @package ChildTheme\Service
 * @author Vincent Ragosta <vincentpasqualeragosta@gmail.com>
 * @version 1.0
 */
class Service extends PostBase
{
    const POST_TYPE = 'service';
}
