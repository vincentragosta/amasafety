<?php

namespace ChildTheme\Controller;

/**
 * Class VcLibraryController
 * @package ChildTheme\Controller
 * @author Vincent Ragosta <vincentpasqualeragosta@gmail.com>
 * @version 1.0
 */
class VcLibraryController
{
    protected static $component_blacklist = [
        'content_card',
        'content_block',
        'callout',
        'sub-navigation',
        'ticket_calendar',
        'talent_card',
        'media_carousel',
        'image_carousel',
        'sit_video'
    ];

    protected static $additional_background_colors = [
        'Primary' => 'primary'
    ];

    public function __construct()
    {
        add_filter('backstage/vc-library/blacklist', [$this, 'blacklist']);
        add_filter('backstage/vc-library/background-colors/vc_section', [$this, 'addBackgroundColors']);
    }

    public function blacklist($components)
    {
        return array_merge($components, static::$component_blacklist);
    }

    public function addBackgroundColors($colors) {
        return array_merge($colors, static::$additional_background_colors);
    }
}
