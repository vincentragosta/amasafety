<?php

namespace ChildTheme\Controller;

use Backstage\Controller\RestController;
use ChildTheme\Exception\EmailException;
use ChildTheme\Options\GlobalOptions;
use \WP_REST_Request;

/**
 * Class FormRestController
 * @package ChildTheme\Controller
 * @author Vincent Ragosta <vincentpasqualeragosta@gmail.com>
 * @version 1.0
 *
 * @property string $namespace
 */
class FormRestController extends RestController
{
    const CONTACT_FORM_MESSAGE = "
        Hello AMASafety,
        
        Full Name: %s
        Email Address: %s
        Phone Number: %s
        Company Name: %s
        Company Size: %s
        Industry: %s
        Location: %s
        Preferred Method of Contact: %s
        
        Specific Needs:
        Forklift: %s
        Ladder: %s
        Permit Required Confined Space Entry: %s
        Inspections: %s
        OSHA 10: %s
        OSHA 30: %s
        PPE: %s
        BBC: %s
        Other: %s
        
        Message: %s";

    const HR_FORM_MESSAGE = "
        Hello AMASafety,
        
        Full Name: %s
        Email Address: %s
        Company Name: %s
        
        HR Types:
        Manuals: %s
        Audits: %s
        Training: %s
        Diversity Training: %s
        HIPPA Laws: %s
        Alcohol and Drug Programs: %s
        Other: %s
        
        Message: %s";

    protected $namespace = 'form';

    public function registerRoutes()
    {
        $this->addCreateRoute('/contact/send', [$this, 'sendContactFormEmail']);
        $this->addCreateRoute('/hr/send', [$this, 'sendHRFormEmail']);
    }

    public function sendContactFormEmail(WP_Rest_Request $request)
    {
        if (empty($subject = $request->get_param('subject'))) {
            throw new EmailException('There subject was not sent in the request.', 400);
        }

        if (empty($full_name = $request->get_param('fullName'))) {
            throw new EmailException('There full name was not sent in the request.', 400);
        }

        if (empty($email = $request->get_param('email'))) {
            throw new EmailException('There email was not sent in the request.', 400);
        }

        if (empty($phone = $request->get_param('phone'))) {
            throw new EmailException('The phone number was not sent in the request.', 400);
        }

        if (empty($company_name = $request->get_param('companyName'))) {
            throw new EmailException('The company name was not sent in the request.', 400);
        }

        if (empty($company_size = $request->get_param('companySize'))) {
            throw new EmailException('The company size was not sent in the request.', 400);
        }

        if (empty($industry = $request->get_param('industry'))) {
            throw new EmailException('The industry was not send in the request.', 400);
        }

        if (empty($location = $request->get_param('location'))) {
            throw new EmailException('The location was not send in the request.', 400);
        }

        if (empty($contact = $request->get_param('contact'))) {
            throw new EmailException('The contact was not send in the request.', 400);
        }

        if (empty($to = GlobalOptions::contactEmail())) {
            throw new EmailException('The contact email address is not set.', 400);
        }

        if (!$email_sent = wp_mail(
            $to,
            $subject,
            sprintf(
                static::CONTACT_FORM_MESSAGE,
                $full_name,
                $email,
                $phone,
                $company_name,
                $company_size,
                $industry,
                $location,
                $contact,
                $request->get_param('forklift') ? 'Yes' : 'No',
                $request->get_param('ladder') ? 'Yes' : 'No',
                $request->get_param('prcse') ? 'Yes' : 'No',
                $request->get_param('inspections') ? 'Yes' : 'No',
                $request->get_param('osha10') ? 'Yes' : 'No',
                $request->get_param('osha30') ? 'Yes' : 'No',
                $request->get_param('ppe') ? 'Yes' : 'No',
                $request->get_param('bbc') ? 'Yes' : 'No',
                $request->get_param('other') ? 'Yes' : 'No',
                $request->get_param('message') ? 'Yes' : 'No'
            )
        )) {
            throw new EmailException('The email could not be sent at this time.', 400);
        }
        return ['message' => 'The email was successfully sent.', 'status' => 200];
    }

    public function sendHRFormEmail(WP_Rest_Request $request)
    {
        if (empty($subject = $request->get_param('subject'))) {
            throw new EmailException('The subject was not sent in the request.', 400);
        }

        if (empty($full_name = $request->get_param('fullName'))) {
            throw new EmailException('The full name was not sent in the request.', 400);
        }

        if (empty($email = $request->get_param('email'))) {
            throw new EmailException('The email was not sent in the request.', 400);
        }

        if (empty($company_name = $request->get_param('companyName'))) {
            throw new EmailException('The company name was not sent in the request.', 400);
        }

        if (empty($message = $request->get_param('message'))) {
            throw new EmailException('The message was not send in the request.', 400);
        }

        if (empty($to = GlobalOptions::contactEmail())) {
            throw new EmailException('The contact email address is not set.', 400);
        }

        if (!$email_sent = wp_mail(
            $to,
            $subject,
            sprintf(
                static::HR_FORM_MESSAGE,
                $full_name,
                $email,
                $company_name,
                $request->get_param('manuals') ? 'Yes' : 'No',
                $request->get_param('audits') ? 'Yes' : 'No',
                $request->get_param('training') ? 'Yes' : 'No',
                $request->get_param('diversityTraining') ? 'Yes' : 'No',
                $request->get_param('hippaLaws') ? 'Yes' : 'No',
                $request->get_param('alc-drug-programs') ? 'Yes' : 'No',
                $request->get_param('other') ? 'Yes' : 'No',
                $request->get_param('message') ? 'Yes' : 'No'
            )
        )) {
            throw new EmailException('The email could not be sent at this time.', 400);
        }
        return ['message' => 'The email was successfully sent.', 'status' => 200];
    }
}
