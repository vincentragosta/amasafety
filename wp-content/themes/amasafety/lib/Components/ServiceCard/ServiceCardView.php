<?php

namespace ChildTheme\Components\ServiceCard;

use Backstage\View\Component;
use ChildTheme\Service\Service;

/**
 * Class ServiceCardView
 * @package ChildTheme\Components\ServiceCard
 * @author Vincent Ragosta <vincentpasqualeragosta@gmail.com>
 * @version 1.0
 *
 * @property string $title
 * @property string $url
 * @property string $excerpt
 */
class ServiceCardView extends Component
{
    protected $name = 'service-card';
    protected static $default_properties = [
        'title' => '',
        'url' => [],
        'excerpt' => ''
    ];

    public function __construct(Service $Service)
    {
        parent::__construct([
            'title' => $Service->title(),
            'url' => $Service->permalink(),
            'excerpt'=> $Service->post()->post_excerpt
        ]);
    }
}
