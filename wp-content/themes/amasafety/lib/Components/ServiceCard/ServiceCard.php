<?php

namespace ChildTheme\Components\ServiceCard;

use Backstage\VcLibrary\Support\Component;
use ChildTheme\Service\Service;
use ChildTheme\Service\ServiceRepository;

/**
 * Class ServiceCard
 * @package ChildTheme\Components\ServiceCard
 * @author Vincent Ragosta <vincentpasqualeragosta@gmail.com>
 * @version 1.0
 */
class ServiceCard extends Component
{
    const NAME = 'Service Card';
    const TAG = 'service_card';
    const VIEW = ServiceCardView::class;

    protected $component_config = [
        'description' => 'Display a service in card format.',
        'icon' => 'icon-wpb-toggle-small-expand',
        'wrapper_class' => 'clearfix',
        'is_container' => false,
        'category' => 'Content',
        'params' => [
            'services' => [
                'type' => 'dropdown',
                'heading' => 'Service',
                'param_name' => 'service_id',
                'description' => 'Select a service to display.',
                'admin_label' => true
            ]
        ]
    ];

    protected function setupConfig()
    {
        $options['-- Select Service --'] = '';
        $ServiceRepository = new ServiceRepository();
        $services = $ServiceRepository->findAll();
        foreach($services as $Service) {
            /* @var Service $Service */
            $options[$Service->post()->post_title] = $Service->ID;
        }
        $this->component_config['params']['services']['value'] = $options;
        parent::setupConfig();

    }

    protected function createView(array $atts)
    {
        if (empty($atts['service_id'])) {
            return '';
        }
        /** @var ServiceCardView $ViewClass */
        $ViewClass = static::VIEW;
        return new $ViewClass(new Service($atts['service_id']));
    }
}
