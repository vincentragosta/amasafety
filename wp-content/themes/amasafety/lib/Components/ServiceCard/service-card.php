<?php
/**
 * Expected:
 * @var string $title
 * @var string $url
 * @var string $exerpt
 * @var array $class_modifiers
 * @var array $element_attributes
 */

use Backstage\Util;

if (empty($title) || empty($url)) {
    return '';
}
?>

<div <?= Util::componentAttributes('service-card', $class_modifiers, $element_attributes); ?>>
    <a href="<?= $url; ?>"><h3 class="heading heading--default"><?= $title; ?></h3></a>
    <?php if (!empty($excerpt)): ?>
        <a href="<?= $url; ?>"><p><?= $excerpt; ?></p></a>
    <?php endif; ?>
</div>
