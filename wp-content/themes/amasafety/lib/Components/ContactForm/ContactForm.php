<?php

namespace ChildTheme\Components\ContactForm;


use Backstage\VcLibrary\Support\Component;

/**
 * Class ContactForm
 * @package ChildTheme\Components\ContactForm
 * @author Vincent Ragosta <vincentpasqualeragosta@gmail.com>
 * @version 1.0
 */
class ContactForm extends Component
{
    const NAME = 'Contact Form';
    const TAG = 'contact_form';
    const VIEW = ContactFormView::class;

    protected $component_config = [
        'description' => 'Display the contact form.',
        'icon' => 'icon-wpb-toggle-small-expand',
        'wrapper_class' => 'clearfix',
        'is_container' => false,
        'category' => 'Content',
        'params' => [
            [
                'type' => 'textfield',
                'heading' => 'Form Title',
                'param_name' => 'form_title',
                'description' => 'Add a form title',
                'admin_label' => true
            ]
        ]
    ];

    protected function createView(array $atts)
    {
        /** @var ContactFormView $ViewClass */
        $ViewClass = static::VIEW;
        return new $ViewClass($atts['form_title']);
    }
}
