<?php

namespace ChildTheme\Components\ContactForm;

use Backstage\View\Component;
use ChildTheme\Service\Service;

/**
 * Class ContactFormView
 * @package ChildTheme\Components\ContactForm
 * @author Vincent Ragosta <vincentpasqualeragosta@gmail.com>
 * @version 1.0
 *
 * @property string $title
 */
class ContactFormView extends Component
{
    protected $name = 'contact-form';
    protected static $default_properties = [
        'title' => '',
    ];

    public function __construct(string $title)
    {
        parent::__construct(compact('title'));
    }
}
