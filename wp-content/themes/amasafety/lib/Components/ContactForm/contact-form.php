<?php
/**
 * Expected:
 * @var string $title
 */
?>

<form class="custom-form" id="contact-us-form" action="" method="post">
    <?php if (!empty($title)): ?>
        <h2 class="custom-form__title heading heading--large"><?= $title; ?></h2>
    <?php endif; ?>
    <p class="custom-form__name-container">
        <label for="custom-form__name">Name</label>
        <input type="text" class="custom-form__name" name="contact-us-form__name" placeholder="Name" required="">
    </p>
    <p class="custom-form__message-container">
        <label for="custom-form__message">Message</label>
        <textarea type="text" class="custom-form__message" name="contact-us-form__message" required="">Write your message here</textarea>
    </p>
    <p class="custom-form__submit-container">
        <input type="submit" name="custom-form__submit" id="custom-form__submit" class="button" value="Submit">
    </p>
</form>
