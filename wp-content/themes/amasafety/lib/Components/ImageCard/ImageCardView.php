<?php

namespace ChildTheme\Components\ImageCard;

use Backstage\View\Component;
use WP_Image;

/**
 * Class ImageCardView
 * @package ChildTheme\Components\ImageCard
 * @author Vincent Ragosta <vincentpasqualeragosta@gmail.com>
 * @version 1.0
 *
 * @property WP_Image|bool $image
 * @property string $headline
 * @property array $link
 */
class ImageCardView extends Component
{
    protected $name = 'image-card';
    protected static $default_properties = [
        'image' => false,
        'headline' => '',
        'link' => []
    ];

    public function __construct($image, $headline, $link = [])
    {
        parent::__construct(compact('image', 'headline', 'link'));
    }
}
