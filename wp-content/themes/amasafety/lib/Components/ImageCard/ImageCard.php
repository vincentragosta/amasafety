<?php

namespace ChildTheme\Components\ImageCard;

use Backstage\VcLibrary\Support\Component;
use Backstage\View\Link;

/**
 * Class ImageCard
 * @package ChildTheme\Components\ImageCard
 * @author Vincent Ragosta <vincentpasqualeragosta@gmail.com>
 * @version 1.0
 */
class ImageCard extends Component
{
    const NAME = 'Image Card';
    const TAG = 'image_card';
    const VIEW = ImageCardView::class;

    protected $component_config = [
        'description' => 'Enter an image with static content.',
        'icon' => 'icon-wpb-toggle-small-expand',
        'wrapper_class' => 'clearfix',
        'is_container' => false,
        'category' => 'Content',
        'params' => [
            [
                'type' => 'attach_image',
                'heading' => 'Image',
                'param_name' => 'image_id',
                'value' => '',
                'description' => 'Set the image for this card.',
                'group' => 'Image'
            ],
            [
                'type' => 'textfield',
                'heading' => 'Headline',
                'param_name' => 'headline',
                'value' => '',
                'description' => 'Enter a headline.',
                'admin_label' => true
            ],
            [
                'type' => 'vc_link',
                'heading' => 'Link',
                'param_name' => 'link',
                'value' => '',
                'description' => 'Enter a link.',
            ]
        ]
    ];

    protected function createView(array $atts)
    {
        /** @var ImageCardView $ViewClass */
        $ViewClass = static::VIEW;
        return new $ViewClass(\WP_Image::get_by_attachment_id($atts['image_id']), $atts['headline'], vc_build_link($atts['link']));
    }
}
