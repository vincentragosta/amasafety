<?php
/**
 * Expected:
 * @var WP_Image|bool $image
 * @var string $headline
 * @var array $link
 * @var array $class_modifiers
 * @var array $element_attributes
 */

use Backstage\Util;
use Backstage\View\Link;

if (!$image instanceof WP_Image && empty($headline)) {
    return;
}
?>

<div <?= Util::componentAttributes('image-card', $class_modifiers, $element_attributes); ?>>
    <?= $image->css_class('image-card__image'); ?>
    <div class="image-card__overlay">
        <h2 class="image-card__heading heading heading--default heading--inverted"><?= $headline; ?></h2>
        <hr class="image-card__hr" />
        <?php if (!empty($link) && isset($link['url'])): ?>
            <?= Link::createFromField($link)->class('image-card__link button'); ?>
        <?php endif; ?>
    </div>
</div>
