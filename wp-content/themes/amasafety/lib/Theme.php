<?php

namespace ChildTheme;

use Orchestrator\Theme as ThemeBase;
use ChildTheme\Components;
use ChildTheme\Controller;

/**
 * Class Theme
 *
 * Configure settings by overriding parent class constants
 *
 * @package Theme
 */
class Theme extends ThemeBase
{
    const REMOVE_DEFAULT_POST_TYPE = true;

    const PLATFORM_THEME_SUPPORT = [
        'set-design/nav-menu',
        'design-producer',
        'preview-producer'
    ];

    const EXTENSIONS = [
        Components\Card\Card::class,
        Components\ImageCard\ImageCard::class,
        Components\ServiceCard\ServiceCard::class,
        Components\ContactForm\ContactForm::class,
        Controller\VcLibraryController::class,
        Controller\FormRestController::class
    ];

    /**
     * Add theme-specific hooks
     */
    public function __construct()
    {
        parent::__construct();
        add_filter('global_js_vars', [$this, 'updateJsVars'], 10, 1);
    }

    /**
     * Add theme-specific style and script enqueues
     */
    public function assets()
    {
        parent::assets();
        wp_enqueue_style('google-fonts', 'https://fonts.googleapis.com/css2?family=Poppins:wght@300;500;700&display=swap', '', null);
    }

    public function updateJSVars($js_vars)
    {
        $args = [];
        if (defined('BASIC_AUTH_CREDENTIALS')) {
            $args['basic_auth'] = BASIC_AUTH_CREDENTIALS;
        }
        return array_merge($js_vars, $args);
    }
}
