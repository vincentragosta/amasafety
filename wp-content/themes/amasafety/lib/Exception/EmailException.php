<?php

namespace ChildTheme\Exception;

use Exception;

/**
 * Class EmailException
 * @package ChildTheme\Exception
 * @author Vincent Ragosta <vincentpasqualeragosta@gmail.com>
 * @version 1.0
 */
class EmailException extends Exception {}
