<?php

namespace ChildTheme\Options;

use Backstage\Models\OptionsBase;
use Backstage\SetDesign\SocialIcons\SocialIcon;
use WP_Image;

/**
 * Class GlobalOptions
 * @package ChildTheme\Options
 * @author Vincent Ragosta <vincentpasqualeragosta@gmail.com>
 * @version 1.0
 *
 * @method static WP_Image headerBrandImage()
 * @method static portalUrl()
 * @method static naspUrl()
 * @method static contactEmail()
 * @method static contactPhone()
 * @method static socialIcons()
 */
class GlobalOptions extends OptionsBase
{
    protected $default_values = [
        'header__brand_image' => null,
        'portal_url' => '',
        'nasp_url' => '',
        'contact_email' => '',
        'contact_phone' => '',
        'social_icons' => [],
    ];

    protected function getHeaderBrandImage()
    {
        $header_image = $this->get('header__brand_image');
        return WP_Image::get_by_attachment_id($header_image);
    }

    protected function getSocialIcons()
    {
        return array_map(function($row) {
            $label = isset($row['link']['title']) ? $row['link']['title'] : ucfirst($row['icon']);
            return new SocialIcon($row['icon'], $row['link']['url'] ?: '', $label, []);
        }, $this->get('social_icons'));
    }
}
