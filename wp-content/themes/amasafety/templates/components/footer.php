<?php

use Backstage\SetDesign\Modal\ModalView;
use Backstage\SetDesign\NavMenu\NavMenuView;
use Backstage\SetDesign\SocialIcons\SocialIconsView;
use Backstage\Util;
use ChildTheme\Options\GlobalOptions;
?>

<footer class="footer-nav">
    <div class="footer-nav__container container">
        <div class="footer-nav__row row">
            <div class="footer-nav__column col-12 col-sm-4">
                <div class="column-text">
                    <h3 class="heading heading--default">Hours of Operation</h3>
                    <p>Monday - Friday</p>
                    <p>9AM - 6PM EST</p>
                </div>
                <?php if (has_nav_menu('primary_navigation')): ?>
                    <div class="footer-nav__menu">
                        <?= NavMenuView::createList('primary_navigation'); ?>
                    </div>
                <?php endif; ?>
                <a href="#contact-form-modal" class="button">Request Your Quote</a>
            </div>
            <div class="footer-nav__column col-12 offset-sm-2 col-sm-6">
                <h2 class="heading heading--default">Follow Us</h2>
                <p>Let us be social</p>
                <?php if (!empty($social_icons = GlobalOptions::socialIcons())): ?>
                    <?= SocialIconsView::create(GlobalOptions::socialIcons()); ?>
                <?php endif; ?>
                <?php if ($nasp_url = GlobalOptions::naspUrl()): ?>
                    <a href="<?= $nasp_url; ?>" target="_blank">
                        <img class="footer-nav__nasp" src="<?= Util::getAssetPath('images/nasp.png'); ?>" />
                    </a>
                <?php endif; ?>
            </div>
        </div>
    </div>
</footer>

<?= ModalView::load('contact-form-modal', 'box', Util::getTemplateScoped('templates/components/contact-form.php')); ?>
<?= ModalView::load('hr-form-modal', 'box', Util::getTemplateScoped('templates/components/hr-form.php')); ?>
<?= ModalView::unloadAll(); ?>
