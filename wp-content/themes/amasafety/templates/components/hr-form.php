<form class="custom-form" id="hr-form" action="" method="post">
    <h2 class="custom-form__title heading heading--large heading--inverted">HR Form</h2>
    <fieldset class="custom-form__field">
        <label for="hr-form__full-name">Full Name*</label>
        <input type="text" class="custom-form__input" id="hr-form__full-name" name="hr-form__full-name" placeholder="Full Name" required />
    </fieldset>
    <fieldset class="custom-form__field">
        <label for="hr-form__email">Email Address*</label>
        <input type="email" class="custom-form__input" id="hr-form__email" name="hr-form__email" placeholder="Email Address" required />
    </fieldset>
    <fieldset class="custom-form__field custom-form__field--full-width">
        <label for="hr-form__company-name">Company Name*</label>
        <input type="text" class="custom-form__input" id="hr-form__company-name" name="hr-form__company-name" placeholder="Company Name" required />
    </fieldset>
    <fieldset class="custom-form__field custom-form__field--full-width">
        <label>HR Types</label>
        <div class="custom-form__checkbox-container">
            <label for="hr-form__hr-types--manuals">
                <input type="checkbox" class="custom-form__checkbox" id="hr-form__hr-types--manuals" name="hr-form__hr-types--manuals" /> Manuals
            </label>
            <label for="hr-form__hr-types--audits">
                <input type="checkbox" class="custom-form__checkbox" id="hr-form__hr-types--audits" name="hr-form__hr-types--audits" /> Audits
            </label>
            <label for="hr-form__hr-types--training">
                <input type="checkbox" class="custom-form__checkbox" id="hr-form__hr-types--training" name="hr-form__hr-types--training" /> Training
            </label>
            <label for="hr-form__hr-types--diversity-training">
                <input type="checkbox" class="custom-form__checkbox" id="hr-form__hr-types--diversity-training" name="hr-form__hr-types--diversity-training" /> Diversity Training
            </label>
            <label for="hr-form__hr-types--hippa-laws">
                <input type="checkbox" class="custom-form__checkbox" id="hr-form__hr-types--hippa-laws" name="hr-form__hr-types--hippa-laws" /> HIPAA Laws
            </label>
            <label for="hr-form__hr-types--alc-drug-programs">
                <input type="checkbox" class="custom-form__checkbox" id="hr-form__hr-types--alc-drug-programs" name="hr-form__hr-types--alc-drug-programs" /> Alcohol and Drug Programs
            </label>
            <label for="hr-form__hr-types--other">
                Other<input type="text" class="custom-form__checkbox" id="hr-form__hr-types--other" name="hr-form__hr-types--other" />
            </label>
        </div>
    </fieldset>
    <fieldset class="custom-form__field custom-form__field--full-width">
        <label for="hr-form__message">Message</label>
        <textarea type="text" class="custom-form__textarea" id="hr-form__message" name="hr-form__message"></textarea>
    </fieldset>
    <fieldset class="custom-form__field custom-form__field--full-width">
        <input type="submit" class="custom-form__submit button" id="hr-form__submit" name="hr-form__submit" value="Submit" />
    </fieldset>
</form>
