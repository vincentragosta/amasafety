<form class="custom-form" id="contact-form" action="" method="post">
    <h2 class="custom-form__title heading heading--large heading--inverted">Request A Quote</h2>
    <fieldset class="custom-form__field">
        <label for="contact-form__full-name">Full Name*</label>
        <input type="text" class="custom-form__input" id="contact-form__full-name" name="contact-form__full-name" placeholder="Full Name" required>
    </fieldset>
    <fieldset class="custom-form__field">
        <label for="contact-form__email">Email Address*</label>
        <input type="email" class="custom-form__input" id="contact-form__email" name="contact-form__email" placeholder="Email Address" required />
    </fieldset>
    <fieldset class="custom-form__field">
        <label for="contact-form__phone">Phone Number*</label>
        <input type="text" class="custom-form__input" id="contact-form__phone" name="contact-form__phone" placeholder="Phone Number" required />
    </fieldset>
    <fieldset class="custom-form__field">
        <label for="contact-form__company-size">Number of Participants*</label>
        <input type="text" class="custom-form__input" id="contact-form__company-size" name="contact-form__company-size" placeholder="Number of Participants" required />
    </fieldset>
    <fieldset class="custom-form__field">
        <label for="contact-form__location">Location*</label>
        <input type="text" class="custom-form__input" id="contact-form__location" name="contact-form__location" placeholder="Location" required />
    </fieldset>
    <fieldset class="custom-form__field">
        <label for="contact-form__contact">Preferred Method Of Contact*</label>
        <select class="custom-form__input" id="contact-form__contact" name="contact-form__contact" required>
            <option value="phone">Phone</option>
            <option value="email">Email</option>
        </select>
    </fieldset>
    <fieldset class="custom-form__field">
        <label for="contact-form__company-name">Company Name</label>
        <input type="text" class="custom-form__input" id="contact-form__company-name" name="contact-form__company-name" placeholder="Company Name" />
    </fieldset>
    <fieldset class="custom-form__field">
        <label for="contact-form__industry">Industry</label>
        <input type="text" class="custom-form__input" id="contact-form__industry" name="contact-form__industry" placeholder="Industry" required />
    </fieldset>
    <fieldset class="custom-form__field custom-form__field--full-width">
        <label>Specific Needs</label>
        <div class="custom-form__checkbox-container">
            <label for="contact-form__specific-needs--forklift">
                <input type="checkbox" class="custom-form__checkbox" id="contact-form__specific-needs--forklift" name="contact-form__specific-needs--forklift" /> Forklift
            </label>
            <label for="contact-form__specific-needs--ladder">
                <input type="checkbox" class="custom-form__checkbox" id="contact-form__specific-needs--ladder" name="contact-form__specific-needs--ladder" /> Ladder
            </label>
            <label for="contact-form__specific-needs--prcse">
                <input type="checkbox" class="custom-form__checkbox" id="contact-form__specific-needs--prcse" name="contact-form__specific-needs--prcse" /> Permit Required Confined Space Entry
            </label>
            <label for="contact-form__specific-needs--inspections">
                <input type="checkbox" class="custom-form__checkbox" id="contact-form__specific-needs--inspections" name="contact-form__specific-needs--inspections" /> Inspections
            </label>
            <label for="contact-form__specific-needs--osha-10">
                <input type="checkbox" class="custom-form__checkbox" id="contact-form__specific-needs--osha-10" name="contact-form__specific-needs--osha-10" /> OSHA 10
            </label>
            <label for="contact-form__specific-needs--osha-30">
                <input type="checkbox" class="custom-form__checkbox" id="contact-form__specific-needs--osha-30" name="contact-form__specific-needs--osha-30" /> OSHA 30
            </label>
            <label for="contact-form__specific-needs--ppe">
                <input type="checkbox" class="custom-form__checkbox" id="contact-form__specific-needs--ppe" name="contact-form__specific-needs--ppe" /> Personal Protective Equipment
            </label>
            <label for="contact-form__specific-needs--bbc">
                <input type="checkbox" class="custom-form__checkbox" id="contact-form__specific-needs--bbc" name="contact-form__specific-needs--bbc" /> Bloodborne Pathogens
            </label>
            <label for="contact-form__specific-needs--other">
                Other<input type="text" class="custom-form__checkbox" id="contact-form__specific-needs--other" name="contact-form__specific-needs--other" />
            </label>
        </div>
    </fieldset>
    <fieldset class="custom-form__field custom-form__field--full-width">
        <label for="contact-form__message">Message</label>
        <textarea type="text" class="custom-form__input" id="contact-form__message" name="contact-form__message"></textarea>
    </fieldset>
    <fieldset class="custom-form__field">
        <input type="submit" class="custom-form__submit button" id="contact-form__submit" name="contact-form__submit" value="Submit">
    </fieldset>
</form>
