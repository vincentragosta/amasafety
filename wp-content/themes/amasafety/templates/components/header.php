<?php

use Backstage\SetDesign\Icon\IconView;
use Backstage\SetDesign\NavMenu\NavMenuView;
use Backstage\SetDesign\SocialIcons\SocialIconsView;
use ChildTheme\Options\GlobalOptions;
?>

<header class="header-nav sticky-header" data-gtm="Header">
    <div class="header-nav__container container">
        <div class="header-nav__bar">
            <ul class="header-nav__list list--inline">
                <?php if ($phone = GlobalOptions::contactPhone()): ?>
                    <li><a href="tel:<?= $phone; ?>"><?= $phone; ?></a></li>
                <?php endif; ?>
                <?php if ($email = GlobalOptions::contactEmail()): ?>
                    <li><a href="mailto:<?= $email; ?>"><?= $email; ?></a></li>
                <?php endif; ?>
            </ul>
            <div class="header-nav__social-container">
                <?php if (!empty($social_icons = GlobalOptions::socialIcons())): ?>
                    <?= SocialIconsView::create(GlobalOptions::socialIcons()); ?>
                <?php endif; ?>
                <?php if (!empty($portal_url = GlobalOptions::portalUrl())): ?>
                    <hr class="vertical-line" />
                    <div class="header-nav__portal-container">
                        <a href="<?= $portal_url; ?>" class="header-nav__portal-link" target="_blank"><?= new IconView(['icon_name' => 'portal']); ?> Portal</a>
                    </div>
                <?php endif; ?>
            </div>
        </div>
        <div class="header-nav__main">
            <div class="header-nav__brand">
                <a class="header-nav__brand-link" href="<?= home_url() ?>">
                    <?php if ($header_image = GlobalOptions::headerBrandImage()): ?>
                        <?= $header_image->css_class('header-nav__brand-image') ?>
                    <?php else: ?>
                        <strong><?php bloginfo('name'); ?></strong>
                    <?php endif; ?>
                    <span class="heading heading--default">AMA Safety</span>
                </a>
            </div>
            <?php if (has_nav_menu('primary_navigation')): ?>
                <div class="header-nav__menu">
                    <?= NavMenuView::createResponsive('primary_navigation'); ?>
                </div>
            <?php endif; ?>
        </div>
    </div>
</header>
