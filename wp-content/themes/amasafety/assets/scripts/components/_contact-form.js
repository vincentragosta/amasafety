addAction(INIT, function() {
   var $contactForm = $('#contact-form');
   if (!$contactForm.length) {
       return;
   }
    $contactForm.submit(function (e) {
        e.preventDefault();

        var $modal = $('#contact-form-modal');
        $.ajax({
            method: 'POST',
            url: sit.api_url + 'form/contact/send',
            data: {
                subject: 'AMASafety Quote Requested',
                fullName: $('input[name="contact-form__full-name"]').val(),
                email: $('input[name="contact-form__email"]').val(),
                phone: $('input[name="contact-form__phone"]').val(),
                companyName: $('input[name="contact-form__company-name"]').val(),
                companySize: $('input[name="contact-form__company-size"]').val(),
                industry: $('input[name="contact-form__industry"]').val(),
                location: $('input[name="contact-form__location"]').val(),
                contact: $('#contact-form__contact option:selected').val(),
                forklift: $('input[name="contact-form__specific-needs--forklift"]').is(':checked'),
                ladder: $('input[name="contact-form__specific-needs--ladder"]').is(':checked'),
                prcse: $('input[name="contact-form__specific-needs--prcse"]').is(':checked'),
                inspections: $('input[name="contact-form__specific-needs--inspections"]').is(':checked'),
                osha10: $('input[name="contact-form__specific-needs--osha10"]').is(':checked'),
                osha30: $('input[name="contact-form__specific-needs--osha30"]').is(':checked'),
                ppe: $('input[name="contact-form__specific-needs--ppe"]').is(':checked'),
                bbc: $('input[name="contact-form__specific-needs--bbc"]').is(':checked'),
                other: $('input[name="contact-form__specific-needs--other"]').val(),
                message: $('textarea[name="contact-form__message"]').val()
            },
            beforeSend: function (xhr) {
                xhr.setRequestHeader('X-WP-Nonce', sit.nonce);
                xhr.setRequestHeader('Authorization', 'Basic ' + sit.basic_auth);
            },
            success: function (response) {
                doAction('hideModal', $modal);
            },
            error: function (response) {
                doAction('hideModal', $modal);
            },
            fail: function (response) {
                doAction('hideModal', $modal);
            }
        });
    });
});
