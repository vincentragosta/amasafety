addAction(INIT, function() {
    var $contactUsForm = $('#contact-us-form');
    if (!$contactUsForm.length) {
        return;
    }
    $contactUsForm.submit(function (e) {
        e.preventDefault();

        $.ajax({
            method: 'POST',
            url: sit.api_url + 'form/send',
            data: {
                name: $('input[name="contact-us-form__name"]').val(),
                subject: 'Contact Us',
                message: $('input[name="contact-us-form__message"]').val()
            },
            beforeSend: function (xhr) {
                xhr.setRequestHeader('X-WP-Nonce', sit.nonce);
                xhr.setRequestHeader('Authorization', 'Basic ' + sit.basic_auth);
            },
            success: function (response) {},
            error: function (response) {},
            fail: function (response) {}
        });

    });
});
