addAction(INIT, function() {
    var $hrForm = $('#hr-form');
    console.log($hrForm);
    if (!$hrForm.length) {
        return;
    }
    $hrForm.submit(function (e) {
        e.preventDefault();

        var $modal = $('#hr-form-modal');
        $.ajax({
            method: 'POST',
            url: sit.api_url + 'form/hr/send',
            data: {
                subject: 'AMASafety HR Form Inquiry',
                fullName: $('input[name="hr-form__full-name"]').val(),
                email: $('input[name="hr-form__email"]').val(),
                companyName: $('input[name="hr-form__company-name"]').val(),
                manuals: $('input[name="hr-form__hr-types--manuals"]').is(':checked'),
                audits: $('input[name="hr-form__hr-types--audits"]').is(':checked'),
                training: $('input[name="hr-form__hr-types--training"]').is(':checked'),
                diversityTraining: $('input[name="hr-form__hr-types--diversity-training"]').is(':checked'),
                hippaLaws: $('input[name="hr-form__hr-types--hippa-laws"]').is(':checked'),
                alcDrugPrograms: $('input[name="hr-form__hr-types--alc-drug-programs"]').is(':checked'),
                other: $('input[name="hr-form__hr-types--other"]').val(),
                message: $('textarea[name="hr-form__message"]').val()
            },
            beforeSend: function (xhr) {
                xhr.setRequestHeader('X-WP-Nonce', sit.nonce);
                xhr.setRequestHeader('Authorization', 'Basic ' + sit.basic_auth);
            },
            success: function (response) {
                doAction('hideModal', $modal);
            },
            error: function (response) {
                doAction('hideModal', $modal);
            },
            fail: function (response) {
                doAction('hideModal', $modal);
            }
        });
    });
});
